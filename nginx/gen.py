#!/usr/bin/env python
# compatible with python 2 and 3

# syntax:
# python gen.py [ dev | prod ]
 
import sys
from jinja2 import Template

DEV = True  # default, if 'dev'/'prod' not provided on command line
DOMAIN_LIST='./conf.d/domains.txt'
BACKEND_LIST='./conf.d/backends.txt'
templates=[ './conf.d/host.conf.tmpl',
            './conf.d/proxy_set_header.inc.tmpl',
            './conf.d/enable_cors.inc.tmpl' ]

# return all non-blank, non-commented lines of the file
def readlines(f):
    values = []
    for line in open(f):
        s = line.strip()
        if s and s[0] != '#':
            values.append(s)
    return values


if __name__=='__main__':
    if len(sys.argv) == 2:
        if sys.argv[1] == 'dev':
            DEV= True
        elif sys.argv[1] == 'prod':
            DEV = False

    context = {
        'domains': readlines(DOMAIN_LIST),
        'nuxeo_backends': readlines(BACKEND_LIST),
        'DEV': DEV
    }
    for f in templates:
        template = Template(open(f).read())
        with open(f[:-5],'w') as out:
            out.write(template.render(context))

